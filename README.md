# WikiDataMSL - Many-sorted logics for Wikidata

## Contents

1. javascript code to implement the sort operations for each module (sort.js, validity_module.js, ...)
2. msl-tools/context-maker.py: a tool to create a JSON representation of each context dimension (valididty, causality, ...)
3. msl-tools/mslc.py: a rule and constraint compiler, transforms a MSL formula into a SPARQL CONSTRUCT or SELECT statement 

The implementation principles are formally described in implementMSL.lyx

## Installation and use

Compiler:

    % pip3 install parsimonious
    
    % python3 mslc.py rule-file
    --   compiles rule-file and produces a SPARQL construct or select query 
    --   on the standard output
    

### Versions

#### 1.1

An empty sort value can now be represented by the absence of a context triple, or by an explicit empty value 

The rule compiler has been modified to introduce OPTINAL parts for every sort property