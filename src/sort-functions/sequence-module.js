PREFIX extfn: <http://www.ontotext.com/js#>
    INSERT DATA {
        [] <http://www.ontotext.com/js#register> 
'''

///// module Sequence
//
//  prov. represented by {"replacedby" : ..., "replaces": ...}


function emptySequnce(){
    //return '{"replacedby": "", "replaces": "", "ordinal": ""}' 
    //var es =  {replacedby:"",replaces:""}
    return JSON.stringify({})
}

function hasPrevious(sseq){
    var seq = JSON.parse(sseq)
    if ('replaces' in seq) return seq.replaces != ""
    else return false
}

function hasNext(sseq){
    var seq = JSON.parse(sseq)
    if ('replaceBy' in seq) return seq.replacedby != ""
    else return false
}

function previous(sseq){
    var seq = JSON.parse(sseq)
    if ('replaces' in seq){
        iri = seq.replaces
        if (iri == '') return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createIRI("http://undefined.msl")
        else return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createIRI(seq.replaces)
    } 
    else
        return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createIRI("http://undefined.msl")
}

function next(sseq){
    var seq = JSON.parse(sseq)
    if ('replacedBy' in seq)
        return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createIRI(seq.replacedby)
    else
        return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createIRI("http://undefined.msl")
}

function ordinal(sseq){
    var seq = JSON.parse(sseq)
    if ('ordinal' in seq) return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createIRI(seq.ordinal)
    else return false
}

function seqWithNext(srsrc){
    var rsrc = srsrc.toString()
    var seq = {replacedby : rsrc, replaces: ""}
    return JSON.stringify(seq)
}

function seqWithPrev(srsrc){
    var rsrc = srsrc.toString()
    var seq = {replaces : rsrc, replacedby: ""}
    return JSON.stringify(seq)
}

function seq(p, n, o){
    var rp = p.toString()
    var rn = n.toString()
    var so = o.toString()
    var seq = {replacedby : rn, replaces: rp, ordinal: so}
    return JSON.stringify(seq)
}
'''
}

