PREFIX extfn: <http://www.ontotext.com/js#>
    INSERT DATA {
        [] <http://www.ontotext.com/js#register> 
'''

///// module Causality


// op emptyCause : causality

function emptyCause(){
    return JSON.stringify({}) 
}

// op addEndCause : set[entity] * causality -> causality 

function addEndCause(set_of_resources, causality){
    var cc = JSON.parse(causality)
    var sor = JSON.parse(set_of_resources)
    if (! ('endCause' in cc)) cc.endCause = []
    cc.endCause = set_union(cc.endCause, sor)
    var jcc = JSON.stringify(cc)
    return jcc
    // return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(jcc)
}

// op addHasCause : set[entity] * causality -> causality 

function addHasCause(set_of_resources, causality){
    var cc = (JSON.parse(causality))
    var sor = (JSON.parse(set_of_resources))
    if (! ('hasCause' in cc)) cc.hasCause = []
    cc.hasCause = set_union(cc.hasCause, sor)
    var jcc = JSON.stringify(cc)
    return jcc
}

// op getEndCause : causality -> set[entity]

function getEndCause(c){
    oc = JSON.parse(c)
    if ('endCause' in oc) return JSON.stringify(oc.endCause)
    else return JSON.stringify([])
}    

// op getHasCause : causality -> set[entity]

function getHasCause(c){
    oc = JSON.parse(c)
    if ('hasCause' in oc) return JSON.stringify(oc.hasCause)
    else return JSON.stringify([])
}    

// op unionCause : causality * causality -> causality

function unionCause(c1, c2){
    var oc1 = JSON.parse(c1)
    var oc2 = JSON.parse(c2)
    var ec1 = []
    if ('endCause' in oc1) ec1 = oc1.endCause
    var ec2 = []
    if ('endCause' in oc2) ec1 = oc2.endCause
    var hc1 = []
    if ('hasCause' in oc1) hc1 = oc1.hasCause
    var hc2 = []
    if ('hasCause' in oc2) hc2 = oc2.hasCause
    var jc1uc2 = JSON.stringify({endCause: set_union(ec1, ec2), hasCause: set_union(hc1, hc2)})    
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(jc1uc2)
}

// op inverseCause : causality -> causality

function inverseCause(c) {
    var oc = JSON.parse(c)
    var ec = []
    if ('endCause' in oc) ec = oc.endCause
    var hc = []
    if ('hasCause' in oc) hc = oc.hasCause
    oc.endCause = inverseCauseList(ec)
    oc.hasCause = inverseCauseList(hc)
    return JSON.stringify(oc)
}

// op inverseCause : entity -> entity
//
// @param {string} e      a string (not a json string) containing an entity IRI)
function inverseCauseEntity(e) {
    switch (e) {
        case "wd:Q99521170" : return "wd:Q24037741" ;  // death of subject <--> death of subject's spouse
        case "wd:Q24037741" : return "wd:Q99521170" ;
        // other causes that must be inverted
        // ...
        default: return e
    }  
}

function inverseCauseList(l){
    for (var i = 0; i<l.length; i++) l[i] = inverseCauseEntity(l[i])
    return l
}



function isEmptyCause(c){
    co = JSON.parse(c)
    return co.hasCause.length == 0 && co.endCause.length == 0
}

'''
}
