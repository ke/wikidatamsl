PREFIX extfn: <http://www.ontotext.com/js#>
    INSERT DATA {
        [] <http://www.ontotext.com/js#register> '''

// module space

// representation: an empty ("") country or place means every country / every place

function emptySpace(){
    return JSON.stringify({place: "", country: ""})
}

function testIntersectSpace(s1j, s2j){
    var s1 = JSON.parse(s1j)
    var s2 = JSON.parse(s2j)
    return testIntersectSpace_o(s1, s2)
}

function interSpace(s1j, s2j){
    var s1 = JSON.parse(s1j)
    var s2 = JSON.parse(s2j)
    return JSON.stringify(interSpace_o(s1, s2))
}

//////////////// internal functions


function emptySpace_o(){
    return {place: "", country: ""}
}

function testIntersectSpace_o(s1, s2){
    
    if (s1.country != "" && s2.country != "" && s1.country != s2.country) return false
    if (s1.place != "" && s2.place != "" && s1.place != s2.place) return false
    return true

}

function interSpace_o(s1, s2){
    if (s1.country != "" || s2.country != ""){
        if (s1.country == "") return s2
        if (s2.country == "") return s1
        if (s1.country == s2.country) return s1
        return {country: null, place: null} 
    } 
    if (s1.place != "" || s2.place != ""){
        if (s1.place == "") return s2
        if (s2.place == "") return s1
        if (s1.place == s2.place) return s1
        return {country: null, place: null} 
    } 
}
'''
    }


