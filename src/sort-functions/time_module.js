PREFIX extfn: <http://www.ontotext.com/js#>
    INSERT DATA {
        [] <http://www.ontotext.com/js#register> '''

// spec validityInstantTime =

// sort instantTime
// sort duration 
        
// %% generators
//  op undefined : instantTime 
function undefinedInstant(){
    return JSON.stringify("")
}

//  op undefinedDuration: duration 
//  op instant : datavalue -> instantTime
//     @param sdata {string}     a string literal representing a time
function instant(sdata){
    return JSON.stringify(sdata.toString())
}

// op min : instantTime * instantTime -> instantTime
// op max : instantTime * instantTime -> instantTime

// pred equal : instantTime * instantTime
function equal(si1, si2){
    if (si1 == undefined) return false
    if (si2 == undefined) return false
    return new Date(JSON.parse(si1.toString())).getTime() == new Date(JSON.parse(si2.toString())).getTime()
}

 
// op union : instantTime * instantTime -> instantTime
// op inter : instantTime * instantTime -> instantTime

// pred testIntersect : instantTime * instantTime

// pred __<__ : instantTime * instantTime 
// pred __<=__ : instantTime * instantTime 

// op __-__ : instantTime * instantTime -> duration 
// op __+__ : instantTime * duration -> instantTime 



// spec ValidityTimeInterval =

// sort timeInterval

//      represented as {start: "start time", end: "end time"}

// op undefined : timeInterval

// op undefined : timeInterval

function undefinedTime(){
    return emptyTime()
}

function emptyTime(){
    return JSON.stringify({start: "", end: ""})
}

// op interval : instantTime * instantTime -> timeInterval

function interval(st, et){
    var s = JSON.parse(st.toString())
    var e = JSON.parse(et.toString())
    var inter = {start: s, end: e}
    var sinter = JSON.stringify(inter)
    return sinter
}

// op interval : instantTime * duration -> timeInterval

// not yet implemented

// pred equal : timeInterval * timeInterval 
// pred disjoint : timeInterval * timeInterval
// pred inside : instantTime * timeInterval 

function testInsideInterval(t1j, t2j){   // timeinterval * timeinterval -> bool
    var t1 = JSON.parse(t1j)
    var t2 = JSON.parse(t2j)
    return testInsideInterval_o(t1, t2) 
}

// pred testIntersect : timeInterval * timeInterval

function testIntersectInterval(t1j, t2j){   // timeinterval * timeinterval -> bool
    var t1 = JSON.parse(t1j)
    var t2 = JSON.parse(t2j)
    return ((t1.start <= t2.start || t1.start == "") && (t2.start <= t1.end || t1.end == "")) ||
           ((t2.start <= t1.start || t2.start == "") && (t1.start <= t2.end || t2.end == "")) 
}
// op union : timeInterval * timeInterval -> timeInterval
// op interInterval : timeInterval * timeInterval -> timeInterval 

function interInterval(t1j, t2j){
    return JSON.stringify(interInterval_o(JSON.parse(t1j), JSON.parse(t2j)))
}

// op startTime : timeInterval -> instantTime

function startTime(interval){
    var st = JSON.parse(interval).start
    //if (st == undefined || st == "") return  org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral("")
    return  org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(JSON.stringify(st))
}
// op endTime : timeInterval -> instantTime

function endTime(interval){
    var se = JSON.stringify(JSON.parse(interval).end)
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(se)
}

// op duration : timeInterval -> duration









///////////////// Internal functions that work on JS objects

function emptyTime_o(){
    return {start: "", end: ""}
}

/*
* intersection function on time objects (not json)
*/
function testIntersectInterval_o(t1, t2){
    var res = ((t1.start <= t2.start || t1.start == "") && (t2.start <= t1.end || t1.end == "")) ||
           ((t2.start <= t1.start || t2.start == "") && (t1.start <= t2.end || t2.end == "")) 
    return res 
}

function testInsideInterval_o(i1, i2){
     var res = (i1.start >= i2.start || i2.start == "") && (i1.end <= i2.end || i2.end == "")
     return res
}

/*
* precondition: testIntersectInterval_o(t1, t2) == true
*/
function interInterval_o(t1, t2){
    var sta = t1.start
    if (t2.start > sta) sta = t2.start
    var en = t1.end
    if (t2.end != "" && t2.end < en) en = t2.end
    return {start: sta, end: en}
}



'''
}



PREFIX jsfn:<http://www.ontotext.com/js#>
INSERT DATA {
    [] jsfn:remove "interval"
}


PREFIX jsfn:<http://www.ontotext.com/js#>
PREFIX extfn: <http://www.ontotext.com/js#>
SELECT *
WHERE {
    BIND(extfn:instant("1976") as ?y1976)
    BIND(extfn:instant("1988") as ?y1988)
    BIND(extfn:interval(?y1976, ?y1988) as ?i7688)
    BIND(extfn:instant("1979-02-15") as ?y19790215)
    BIND(extfn:instant("1990") as ?y1990)
    BIND(extfn:interval(?y19790215, ?y1990) as ?i7990)
    BIND(extfn:testIntersectInterval(?i7688, ?i7990))
    ?s ?p ?o
}
LIMIT 1