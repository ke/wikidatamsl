PREFIX extfn: <http://www.ontotext.com/js#>
    INSERT DATA {
        [] <http://www.ontotext.com/js#register> '''


// spec ValidityContext[TimeParam][SpaceParam] = 


// sort validityContext


// sort operations defined in the ADT specification
// the parameters and results are JSON strings

// %% Generators 
// op emptyValidity : validityContext %% empty means valid in every context 

function emptyValidity(){

    // return '{"time": {"period": "", "startPeriod": "", "endPeriod": "", "start": "", "end": ""}, "space": {"place": "", "country": ""}}' 
    return JSON.stringify({})
}

// op timeValidity : time -> validityContext
/*
 * @param {string} t     a JSON string encoding a validity time
 */
function timeValidity(t){
    var ot = JSON.parse(t)
    return JSON.stringify({time: ot, space: {}})
}

// op spaceValidity : space -> validityContext
/*
 * @param {string} s     a JSON string encoding a validity space
 */


function spaceValidity(s){
    var os = JSON.parse(s)
    return JSON.stringify({space: os, time: {start: "", end: ""}})
}

// op timespaceValidity : time * space -> validityContext
/*
 * @param {string} t     a JSON string encoding a validity time
 * @param {string} s     a JSON string encoding a validity space
 */
function timespaceValidity(t, s){
    var ot = JSON.parse(t)
    var os = JSON.parse(s)
    return JSON.stringify({space: os, time: ot})
}

// pred testIntersectValidity : validityContext * validityContext 

function testIntersectValidity(v1j, v2j){   // validitycontext * validitycontext -> boolean
    if (v1j == undefined) return false
    if (v2j == undefined) return false
    var v1 = JSON.parse(v1j)
    var v2 = JSON.parse(v2j)
    if (! ('time' in v1)) v1.time = emptyTime_o()
    if (! ('time' in v2)) v2.time = emptyTime_o()
    if (! ('space' in v1)) v1.space = emptySpace_o()
    if (! ('space' in v2)) v2.space = emptySpace_o()    
    var res = testIntersectInterval_o(v1.time, v2.time) && testIntersectSpace_o(v1.space, v2.space)
    return res  // booleans are returned to graphdb as boolean literals
                // b. lit. "true", "false" are also legal JSON values 
}

// pred incl : validityContext * validityContext 
// pred equal : validityContext * validityContext 

// op union : validityContext * validityContext -> validityContext 

// op interValidity : validityContext * validityContext -> validityContext

function interValidity(v1j, v2j){  // validitycontext * validitycontext -> validitycontext
    var v1 = emptyValidity_o()
    if (v1j != undefined) v1 = JSON.parse(v1j)
    var v2 = emptyValidity_o()
    if (v2j != undefined) v2 = JSON.parse(v2j)
    if (! ('time' in v1)) v1.time = emptyTime_o()
    if (! ('time' in v2)) v2.time = emptyTime_o()
    if (! ('space' in v1)) v1.space = emptySpace_o()
    if (! ('space' in v2)) v2.space = emptySpace_o()  
    itime = interInterval_o(v1.time, v2.time)
    ispace = interSpace_o(v1.space, v2.space)
    return JSON.stringify({time: itime, space: ispace})
}

// op extractTime : validityContext -> time

function extractTime(sv){ // extract time
    var v = JSON.parse(sv)
    if ('time' in v){
        var t = v.time
        var st = JSON.stringify(t)
        return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(st)
    } else 
        return emptyTime()
}

// op extractSpace : validityContext -> space
function extractSpace(sv){
    var v = JSON.parse(sv)
    if ('space' in v) {
        var t = v.space
        var st = JSON.stringify(t)
        return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(st)
    } else return emptySpace()
}

// op setTime : validityContext * time -> validityContext

function setTime(vcj, tj){
    var vc = JSON.parse(vcj)
    var t = JSON.parse(tj)
    vc.time = t
    return JSON.stringify(vc)
}

// op setSpace : validityContext * space -> validityContext

function setSpace(vcj, sj){
    var vc = JSON.parse(vcj)
    var s = JSON.parse(sj)
    vc.space = s 
    return JSON.stringify(vc)
}

// Auxiliary functions

function emptyValidity_o(){
    return {time: emptyTime_o(), space: emptySpace_o()}
}

'''
}

