
PREFIX extfn: <http://www.ontotext.com/js#>
    INSERT DATA {
        [] <http://www.ontotext.com/js#register> '''

// module annotation 

// sort operations defined in the ADT specification
// the parameters and results are strings that represent JSON objects

/*
    Representation of an annnotation value :

    {time: time-value, class: [c1, ...], relation: [r1, ...] }
*/

	
function emptyAnnotations(){
    var ea =  {}
    return JSON.stringify(ea)
}


function getTimeAnnot(annotation){
    var a =(JSON.parse(annotation))
    if ('time' in a) return JSON.stringify(a.time)
    else return JSON.stringify({})
}

function getClass(annotation){
    var a =(JSON.parse(annotation))
    if ('class' in a) return JSON.stringify(a.class)
    else return JSON.stringify({})
}

function getRelation(annotation){
    var a =(JSON.parse(annotation))
    if ('relation' in a) return JSON.stringify(a.relation)
    else return JSON.stringify({})
}


'''
}
