PREFIX extfn: <http://www.ontotext.com/js#>
    INSERT DATA {
        [] <http://www.ontotext.com/js#register> '''

/*
   Auxiliary functions
*/
        function ps(uri){} // set the prefix to ps:
        
/*
    Transform an iri wd:x into p:x
 */
function so2p(iri) { // set the prefix of iri to p:
    var piristr = iri.toString().replace('http://www.wikidata.org/entity/','http://www.wikidata.org/prop/')
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createIRI(piristr)
}

function so2ps(iri) { // set the prefix of iri to p:
    var piristr = iri.toString().replace('http://www.wikidata.org/entity/','http://www.wikidata.org/prop/statement/')
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createIRI(piristr)
}

function p2so(iri) { // set the prefix of iri to p:
    var piristr = iri.toString().replace('http://www.wikidata.org/prop/','http://www.wikidata.org/entity/')
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createIRI(piristr)
}

/*
 * @param {list of String} s
 * @param {String} x
 * @return {bool} true iff x is a member of s
 */
function set_contains(s, x){
    for (var i = 0; i<s.length; i++) if (s[i] == x)return true
    return false
}

/*
 * @param {String} ss    a JSON string that represents a list of values sv
 * @param {String} xs    a JSON string that represents a values xv
 * @return {bool}        xv is in sv
 */
function jcontains(ss, xs){
    var res = set_contains(JSON.parse(ss), JSON.parse(xs))
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(res);
}

/*
 * @param {String} ss    a JSON string that represents a list of values sv
 * @param {String} xp    a string from the SPARQL engine (not necessarily a JSON string)
 * @return {bool}        the value represented by xp is in sv
 */
function kcontains(ss, xp){
    x = xp.toString()
    // transform non JSON string into a string representing a string
    if (x[0] != "{" && x[0] != '[' && x[0] != '"' ) x = '"' + x.replace('"','\\"') + '"'; 
    xo = JSON.parse(x)
    var res = set_contains(JSON.parse(ss), xo)
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(res);
}
/*
 * @param {list of String} s
 * @param {list of String} t
 * @return {list String} the union of s and t contents
 * 
 * O(nm) algorithm, but fast enough for small sets
 */
function set_union(s, t){ 
    var r = []
    for (var i = 0; i<s.length; i++) if (! set_contains(r, s[i])) r.push(s[i])
    for (var i = 0; i<t.length; i++) if (! set_contains(r, t[i])) r.push(t[i])
    return r
}

/*
 * @pre: s and t don't have duplicates
 */
function set_equals(s, t){
    if (s.length != t.length) return false
    for (var i = 0; i<s.length; i++) if (! set_contains(t, s[i])) return false
    return true
}

function junion(a, b) {
    return JSON.stringify(set_union(JSON.parse(a), JSON.parse(b)))
}


'''
}


