PREFIX extfn: <http://www.ontotext.com/js#>
    INSERT DATA {
        [] <http://www.ontotext.com/js#register> 
'''

///// module provenance

// Basic representation of a provenance: {"sources": [list of entities (IRIs)]}

// op emptyProvenance : provenance

function emptyProvenance(){
    return '{}' 

}

// op addSources : set[entity] * provenance -> provenance

function addSources(srcj, pj){
    var src = JSON.parse(srcj)
    var p = JSON.parse(pj)
    var ps = []
    if ("sources" in p) ps = p.sources
    p.sources = set_union(src, ps)
    return JSON.stringify(p)
}

// op getSources : provenance -> set[entity]
function getSources(pj){
    return JSON.stringify(getSourcesO(pj))
}

// op union : provenance * provenance -> provenance

function unionProv(pj, qj){
    var p = getSourcesO(pj)
    var q = getSourcesO(qj)
    var res = JSON.stringify({sources: set_union(p, q)})    
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(res)
}


function getSourcesO(pj){
    var p = JSON.parse(pj)
    if ("sources" in p) return p.sources
    else return []
}

'''
}
