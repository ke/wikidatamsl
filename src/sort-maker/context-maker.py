# from contextvars import Context
# from SPARQLWrapper import SPARQLWrapper, RDFXML, JSON, XML, DIGEST
# from rdflib import Graph
import json
import sys

# abstract
class Context:
    
    # abstract
    def qualifiers(self)  :
        """
        the qualifiers used to creat a value for a context sort
        each qualifier has a meaningful name associated to a wikidata property name or path
        """
        pass
     
    # abstract  
    def  make_j_value(self, qualif_values) -> str :   # qualif_values: dict[str, list[str]]
        """
        build a sort value from qualifier values and return it's json-encoded form
        """
        pass
        

def emptyValue(d: dict[str, list[str]]) -> bool :
    for v in d :
        if v != [] : return False
    return True

class Validity(Context):
    
   def contextProperty(self) : return "pq:validityJ"
    
   # overrides
   def qualifiers(self):
       return {"period": "pq:P1264", "startPeriod": "pq:P3415", "endPeriod": "pq:P3416", 
                   "start": "pq:P580", "end": "pq:P582",   
                   "place": "pq:P3005", "country": "pq:P17"}
   # overrides           
   def make_j_value(self, qualif_values: dict[str, list[str]]) -> str :
        # all the qualifiers are single-valued -> transform the lists of values
        # # to a single value
        isEmpty = True
        for p in qualif_values :
            if qualif_values[p] == [] : 
                qualif_values[p] = ""  
            else: 
                isEmpty = False
                qualif_values[p] = qualif_values[p][0]
        if isEmpty :
            return '{}'
            # return '{"time": {"period": "", "startPeriod": "", "endPeriod": "", "start": "", "end": ""}, "space": {"place": "", "country": ""}}'
        else:
            vobj = {
        "time": {"period": qualif_values["period"],
            "startPeriod": qualif_values["startPeriod"],
            "endPeriod" : qualif_values["endPeriod"],
            "start": qualif_values["start"], 
            "end": qualif_values["end"]},
        "space": {"place": qualif_values["place"],
                "country": qualif_values["country"]}}
            return json.dumps(vobj)
            
   
class Causality(Context):
    
    def contextProperty(self) : return "pq:causalityJ"
    
    # overrides
    def qualifiers(self): 
        return  {"hasCause": "pq:P828", "endCause": "pq:P1534"}

   # overrides
    def make_j_value(self, qualif_values: dict[str, list[str]]) -> str :
        if qualif_values["endCause"] == [] and qualif_values["hasCause"] == []:
            return '{}'    # was '{"endCause": [], "hasCause": []}' 
        o = {"endCause": qualif_values["endCause"], 
            "hasCause": qualif_values["hasCause"]}
        return   json.dumps(o)

class Seq(Context):
    
    def contextProperty(self) : return "pq:sequenceJ"
    
    # overrides
    def qualifiers(self): 
        return  {"replaces": "pq:P1365", "replacedby": "pq:P1366", "ordinal": "pq:P1545"}

   # overrides
    def make_j_value(self, qualif_values: dict[str, list[str]]) -> str :
        if qualif_values["replaces"] == [] and qualif_values["replacedby"] == [] and qualif_values["ordinal"] == [] :
            return '{}' 
        o = {"replaces": qualif_values["replaces"], 
            "replacedby": qualif_values["replacedby"],
            "ordinal": qualif_values["ordinal"]}
        return   json.dumps(o)

class Annotation(Context):
    
    def contextProperty(self) : return "pq:annotationJ"
    
    # overrides
    def qualifiers(self): 
        return  {"time": "pq:P585", "class": "pq:P2308", "relation": "pq:P2309"}

   # overrides
    def make_j_value(self, qualif_values: dict[str, list[str]]) -> str :
        if qualif_values["time"] == [] and qualif_values["class"] == [] and qualif_values["relation"] == [] :
            return '{}' 
        o = {"time": qualif_values["time"], 
            "class": qualif_values["class"],
            "relation": qualif_values["relation"]}
        return   json.dumps(o)
    

class Provenance(Context):
    
    def contextProperty(self) : return "pq:provenanceJ"
    
    # overrides    
    def qualifiers(self):
        return {"objectnamedas": "pq:P1932", "determinationmethod": "pq:P459", "subjectnamedas": "pq:P1810", "criterionused": "pq:P1013",
         "wasderivedfrom": "prov:wasDerivedFrom"}
        #return {"statedIn": "prov:wasDerivedFrom/pr:P248",
        #        "referenceURL":"prov:wasDerivedFrom/pr:P854" }

   # overrides
    def make_j_value(self, qualif_values: dict[str, list[str]]) -> str : 
        if emptyValue(qualif_values) : return '{}'
        # o = {"wasDerivedFrom": qualif_values["wasDerived"]}
        o = {"sources": qualif_values["wasderivedfrom"]}
        #     "objectnamedas": qualif_values["objectnamedas"], 
        #    "determinationmethod": qualif_values["determinationmethod"],
        #    "subjectnamedas": qualif_values["subjectnamedas"],
        #    "criterionused": qualif_values["criterionused"]}
        #used = qualif_values["statedIn"] + qualif_values["referenceURL"] # + others
        #o = {"wasGeneratedBy": "", "used": used}
        return json.dumps(o)


def gen_values(prop: str, c: Context) :
    """
    Generate the context values for a propery and a context sort
    :params prop: the property to contextualize
    :param c: a context sort definition
       
    method:
    1. query the graph to find all the qualifier values for each statement on
       the property to contextualize.
           all the values for a qualifier are concatenated
    2. for each statement send the qualifier values to the maker function
    """
    
    q = """
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX pr: <http://www.wikidata.org/prop/reference/>
SELECT ?s
"""
    qualifiers = c.qualifiers()
    
    for v in qualifiers :
        q += '   (CONCAT("[", GROUP_CONCAT(?' + v + 'iq ; SEPARATOR=", "), "]") AS ?' + v + ')\n'
    q += 'WHERE { ?x p:' + prop + ' ?s  .\n'
    for v in qualifiers :
        q += '   OPTIONAL{?s ' + qualifiers[v] + ' ?' + v + 'i}\n'
        q += '   BIND(CONCAT("\\"", REPLACE(str(?' + v +'i),"\\"","\\\\\\""),"\\"") AS ?' + v + 'iq)\n'
    q += "}\nGROUP BY ?s"
    print(q)
    
    sparql = SPARQLWrapper("http://ke.unige.ch:7200/repositories/wikidata")
    sparql.setCredentials("etu", "x")
    sparql.setQuery(q)
    sparql.setReturnFormat(JSON)
    print('\n--- execute query\n')
    results = sparql.query().convert()
    print('\n--- write output file')
    out = open(prop+'-'+c.contextProperty().replace(':','-')+'-import.ttl', 'w')
    out.write( """
@prefix p: <http://www.wikidata.org/prop/> .
@prefix wd: <http://www.wikidata.org/entity/> .
@prefix wdt: <http://www.wikidata.org/prop/direct/> .
@prefix ps: <http://www.wikidata.org/prop/statement/> .
@prefix pq: <http://www.wikidata.org/prop/qualifier/> .

""")
    
    for result in results["results"]["bindings"]:
        r = {}
        for v in qualifiers:
            r[v] = json.loads(result[v]["value"])
        j = c.make_j_value(r)
        s = result['s']['value']
        s = s.replace('http://www.wikidata.org/entity/statement/','wds:')
        out.write(s + ' ' + c.contextProperty() + ' """' + j + '""".\n')
        # print(result['s']['value'], prop, j)
    
    out.close()

def gen_values_from_pfiles(prop: str, c: Context, idir, odir) :
    """
    Generate the context values for a propery and a context sort
    :params prop: the property to contextualize
    :param c: a context sort definition
       
    method:
    1. read all the qualifier files for this context and create a dictionary statement -> value(s) for each qualifier
    2. read the property (p:) file for this property
            for each statement send the qualifier values to the maker function
    """
    
    qualifiers = c.qualifiers()
    print(c.contextProperty())
    print(qualifiers)
    
    qdict = {}
    for q in qualifiers :
        # <<read the qualifier file for v and build the statement -> value(s) dictionary>>
        # remove the xsd:types and the ""
        
        qdict[q] = {}
        qn = qualifiers[q]
        qn = qn.replace('pq:','pq/')
        # qn = qn.replace('prov:wasDerivedFrom/pr:','pr/')
        qn = qn.replace('prov:wasDerivedFrom','prov/'+prop+'_wasDerivedFrom')
        
        print('reading qualifier '+q+' from file '+qn)
        
        fq = open(idir+'/'+qn+'.nt','r')
        lc = 0
        for line in fq:
            lc += 1
            # find the s, p, o separators
            ends = line.find(' ',0)
            endp = line.find(' ',ends+1)
            s = line[0:ends]
            p = line[ends+1:endp]
            o = line[endp+1:len(line)-3] # every line ends with ' .\n'
            if o[0] == '"':
                lastquote = o.rfind('"')
                o = o[1 : lastquote]
            if s in qdict[q] :
                qdict[q][s] . append(o) # += (', ' + o)
            else:
                qdict[q][s] = [o]
    
    outname = odir + '/' + prop+'-'+c.contextProperty().replace(':','-')+'-import.ttl'
    print('\n--- write output file '+outname)
    out = open(outname, 'w')
    out.write( """
@prefix p: <http://www.wikidata.org/prop/> .
@prefix wd: <http://www.wikidata.org/entity/> .
@prefix wdt: <http://www.wikidata.org/prop/direct/> .
@prefix ps: <http://www.wikidata.org/prop/statement/> .
@prefix pq: <http://www.wikidata.org/prop/qualifier/> .

""")
    # for each statement with this property
    pfile = open(idir+'/p/'+prop+'.nt','r')
    for ptriple in pfile :
        # extract the object (statement)
        ends = ptriple.find(' ',0)
        endp = ptriple.find(' ',ends+1)
        stmt = ptriple[endp+1:len(ptriple)-3] # every line ends with ' .\n'
        r = {}
        for v in qualifiers:
            if stmt in qdict[v] :
                r[v] = qdict[v][stmt]
            else: r[v] = []
        j = c.make_j_value(r)
        out.write(stmt + ' ' + c.contextProperty() + ' """' + j + '""".\n')
    
    out.close()
    



def add_contexts(prop: str, selectContexts: str, method: str, id, od):
    if method == 'f' :
        if 's' in selectContexts : gen_values_from_pfiles(prop, Seq(), id, od)
        if 'v' in selectContexts : gen_values_from_pfiles(prop, Validity(), id, od)
        if 'c' in selectContexts : gen_values_from_pfiles(prop, Causality(), id, od)
        if 'p' in selectContexts : gen_values_from_pfiles(prop, Provenance(), id, od)
        if 'a' in selectContexts : gen_values_from_pfiles(prop, Annotation(), id, od)
    elif method == 'g' :
        if 's' in selectContexts : gen_values(prop, Seq())
        if 'v' in selectContexts : gen_values(prop, Validity())
        if 'c' in selectContexts : gen_values(prop, Causality())
        if 'p' in selectContexts : gen_values(prop, Provenance())
        if 'a' in selectContexts : gen_values(prop, Annotation())
    else:
        print('method must be f or g')



if __name__ == "__main__":

    if len(sys.argv) < 6 :
        print("""
Usage: 
python3 context-maker.py Property ContextSelector MethodSelector InputDir OutputDir
    where 
          ContextSelector belongs to (s|v|c|p|a)+
          MethodSelector is f or g (file or graph)
          InputDir is a directory that has subdirectories triples/p /pq /pr ...
          OutputDir is where to put the result files
""")
    else:
        add_contexts(sys.argv[1], sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5])

