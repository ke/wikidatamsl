# validity context maker
#
# valid in period P1264 restrictive qualifier Item -- can be anything (tv series episode, video game, ...)
# start period    P3415 restrictive qualifier Item -- extremely rare
# end period      P3416 restrictive qualifier Item -- extremely rare
# start time      P580 unqualified Point in time
# end time        P582 unqualified Point in time
#
# valid in place  P3005 
# country         P17 

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
INSERT DATA 
{
        [] <http://www.ontotext.com/js#register> 
'''
function makeValidity(vperiod, startperiod, endperiod, st, et, vplace, country){
  var vobj = {
    time: {period: vperiod.toString(),
           startPeriod: startperiod.toString(),
           endPeriod: endperiod.toString(),
           start: st.toString(), 
           end: et.toString() },

     space: {place: vplace.toString(),
             country: country.toString()}
  }
  var jvobj = JSON.stringify(vobj)
  return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(jvobj);
} 
'''

}

    PREFIX p: <http://www.wikidata.org/prop/>
    PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
    PREFIX extfn: <http://www.ontotext.com/js#>
    PREFIX msl: <http://cui.unige.ch/msl/prop#>
    insert {?stmt pq:validityJ ?vc}
    where      {?x p:P27 ?stmt. 
                optional{?stmt pq:P1264 ?p1264} 
                optional{?stmt pq:P3415 ?p3415} 
                optional{?stmt pq:P3416 ?p3416} 
                optional{?stmt pq:P580 ?p580} 
                optional{?stmt pq:P582 ?p582}
                optional{?stmt pq:P3005 ?p3005}
                optional{?stmt pq:P17 ?p17}
            
            bind (extfn:makeValidity(
                IF(BOUND(?p1264), ?p1264, ""),
                IF(BOUND(?p3415), ?p3415, ""),
                IF(BOUND(?p3416), ?p3416, ""),
                IF(BOUND(?p580), str(?p580), ""),    # bricolage
                IF(BOUND(?p582), str(?p582), ""),
                IF(BOUND(?p3005), ?p3005, ""),
                IF(BOUND(?p17), ?p17, "")
            ) as ?vc)
    }
        

# Causality context maker

# P828  : has cause
# P1534 : end cause

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
INSERT DATA 
{
        [] <http://www.ontotext.com/js#register> 
'''
function makeCausality(ec1, ec2, ec3, hc1, hc2, hc3){
    // var ec = Array.from(new Set([ec1, ec2, ec3]).delete(""))
    var ec = []
    if (ec1 != "") ec.push(ec1.toString())
    if (ec2 != "") ec.push(ec2.toString())
    if (ec3 != "") ec.push(ec3.toString())
    //for (e of [ec1, ec2, ec3]) if (e != "") ec.push(e)
    // var hc = Array.from(new Set([hc1, hc2, hc3]).delete(""))
    var hc = []
    if (hc1 != "") hc.push(hc1.toString())
    if (hc2 != "") hc.push(hc2.toString())
    if (hc3 != "") hc.push(hc3.toString())
    //for (h of [hc1, hc2, hc3]) if (h != "") hc.push(h)
    var c = { hasCause: hc , endCause: ec }
    var jc = JSON.stringify(c)
  
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(jc);
} 
'''

}

# consider at most 3 has and end causes

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
PREFIX msl: <http://cui.unige.ch/msl/prop#>
INSERT {?s pq:causalityJ ?cc}
WHERE { ?x p:P26 ?s . 

    optional {?s pq:P1534 ?ec1} .
    optional{?s pq:P1534 ?ec2 filter (bound(?ec1) && str(?ec1) < str(?ec2))}
    optional{?s pq:P1534 ?ec3 filter (bound(?ec2) && str(?ec2) < str(?ec3))}

    optional {?s pq:P828 ?hc1} .
    optional{?s pq:P828 ?hc2 filter (bound(?hc1) && str(?hc1) < str(?hc2))}
    optional{?s pq:P828 ?hc3 filter (bound(?hc2) && str(?hc2) < str(?hc3))}

    filter(bound(?ec1))
    bind (extfn:makeCausality(
                IF(BOUND(?ec1), ?ec1, ""),
                IF(BOUND(?ec2), ?ec2, ""),
                IF(BOUND(?ec3), ?ec3, ""),
                IF(BOUND(?hc1), ?hc1, ""),
                IF(BOUND(?hc2), ?hc2, ""),
                IF(BOUND(?hc3), ?hc3, "")
            ) as ?cc)
   
} 



# Sequence context maker
#
# replaced by : P1366
# replaces : P1365
# follows : P155
# followed by : P156

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
INSERT DATA 
{
        [] <http://www.ontotext.com/js#register> 
'''
function makeSequence(rpby, rpcs){
    var seq = {replacedby: rpby.toString(), replaces: rpcs.toString()}
    return JSON.stringify(seq)
}
'''
}

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
PREFIX msl: <http://cui.unige.ch/msl/prop#>

INSERT {?stmt pq:sequenceJ ?seq}
WHERE {?x ?p ?stmt 
   OPTIONAL {?stmt pq:P1366 ?rpby}
   OPTIONAL {?stmt pq:P1365 ?rpcs}
   BIND(extfn:makeSequence(
       IF(BOUND(?rpby), ?rpby, ""),
       IF(BOUND(?rpcs), ?rpcs, "")) AS ?seq)

}


# Provenance maker

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
INSERT DATA 
{
        [] <http://www.ontotext.com/js#register> 
'''
function makeProvenance(statedIn1, statedIn2, statedIn3){
    var prov = []
    if (statedIn1 != "") ec.push(statedIn1.toString())
    if (statedIn2 != "") ec.push(statedIn2.toString())
    if (statedIn3 != "") ec.push(statedIn3.toString())
    return JSON.stringify(prov)
}
'''
}


PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
PREFIX msl: <http://cui.unige.ch/msl/prop#>
INSERT {?s pq:provenanceJ ?prov}
WHERE { ?x p:P26 ?s . 

    optional{?s prov:wasDerivedFrom ?ec1. ?ec1 pr:statedIn ?si1} .
    optional{?s prov:wasDerivedFrom ?ec2. ?ec2 pr:statedIn ?si2 filter (bound(?si1) && str(?si1) < str(?si2))}
    optional{?s prov:wasDerivedFrom ?ec3. ?ec3 pr:statedIn ?si3 filter (bound(?si3) && str(?si3) < str(?si3))}

    filter(bound(?ec1))
    bind (extfn:makeProvenance(
                IF(BOUND(?si1), ?si1, ""),
                IF(BOUND(?si2), ?si2, ""),
                IF(BOUND(?si3), ?si3, "")  
            ) as ?prov)
   
} 


///////////////////////////////////////////

//// Annotation maker

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
INSERT DATA 
{
        [] <http://www.ontotext.com/js#register> 
'''
function makeAnnotation(a1, a2, a3, b1, b2, b3, t1, t2, t3){
    // var ec = Array.from(new Set([ec1, ec2, ec3]).delete(""))
    var a = []
    if (a1 != "") a.push(a1.toString())
    if (a2 != "") a.push(a2.toString())
    if (a3 != "") a.push(a3.toString())
    
    var b = []
    if (b1 != "") b.push(b1.toString())
    if (b2 != "") b.push(b2.toString())
    if (b3 != "") b.push(b3.toString())
    
    var t=[]
    if (t1 != "") t.push(t1.toString())
    if (t2 != "") t.push(t2.toString())
    if (t3 != "") t.push(t3.toString())
    var c = { annotation1: a , annotation2: b, time: t }
    var jc = JSON.stringify(c)
  
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(jc);
} 
'''

}


PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
INSERT DATA 
{
        [] <http://www.ontotext.com/js#register> 
'''
function makeAnnotation(t1){
    // var ec = Array.from(new Set([ec1, ec2, ec3]).delete(""))
    
    var t=[]
    if (t1 != "") t.push(t1.toString())
    var c = { time: t }
    var jc = JSON.stringify(c)
  
    return org.eclipse.rdf4j.model.impl.SimpleValueFactory.getInstance().createLiteral(jc);
} 
'''

}

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX extfn: <http://www.ontotext.com/js#>
PREFIX msl: <http://cui.unige.ch/msl/prop#>

INSERT {?s pq:annotationJ ?a}

WHERE { ?x p:P156 ?s .     # followed by

    optional {?s pq:P585 ?t1} .   # point in time
    

    filter(bound(?t1))
    bind (extfn:makeAnnotation(
                IF(BOUND(?t1), ?t1, ""),
            ) as ?a)
   
}
