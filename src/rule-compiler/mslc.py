"""
MSL rule compiler

usage:
$ pip3 install parsimonious
$ python3 mslc.py rulefilename


(c) G. Falquet, CUI, UNIGE, 2022-2023
"""
import sys
import re
from xmlrpc.client import boolean
import parsimonious

from parsimonious.grammar import Grammar
from parsimonious.nodes import NodeVisitor
from parsimonious.nodes import Node

g = Grammar("""
start = ("rule" sp string rule) / ("constraint" sp string rule)
rule = body "->" head sp?
body = sp? statement (sp statement)* (sp condition)* sp?
head = (sp inferredstatement)* (sp condition)* 
statement = ("S"/"st") sp? "(" sp? subj  sp predicate  sp obj  (sp ctxt )* sp? ")" 
subj = var / pred / entity 
predicate = var / pred / entity 
obj = var / pred / entity 
var = ~r"[A-Z][A-Za-z0-9_-]*"
funct = ~r"[a-z][A-Za-z0-9_-]*"
pred = ~r":P[0-9]+(_[^\s^,]+)?"
entity = ~r":Q[0-9]+"
ctxt = var / "ε" 

arguments = expr (sp expr)*
condition = funct sp? "(" arguments ")"  
funcall   = funct sp? "(" arguments? ")"
expr = (var / string / entity / pred / funcall ) 

inferredstatement = ("S"/"st") sp? "(" sp? infsubj  sp infpredicate sp infobj  (sp infctxt)* sp? ")"  
infsubj = expr
infpredicate = expr
infobj = expr
infctxt =  expr / "ε"

string = ("\\"" strcontent "\\"") / ("'" strcontentsq "'" )
strcontent =  ~r"[^\\"]*"
strcontentsq = ~r"[^']*"
spx = (~r"\s*[,\s]\s*") comment*
sp = (~r"\s*[,\s]\s*") (comment (~r"\s*"))*
comment = "%%" commentcontent
commentcontent = ~r"[^\\n]*"
""")

c_types = ['v','c','s','a','p']  # category/context types

so_var: set[str] = set()  # variables in subjet or object positions or in conditions in the body
p_var: set[str] = set()   # variabls in predicate position in the body
head_so_var: set[str] = set()
head_p_var: set[str] = set()

IN_BODY = 1
IN_HEAD = 2

RULE = 1
CONSTRAINT = 2

category = RULE
PROP_PATTERN = re.compile('(P[0-9]+)')

def collect_first_descendents(n: Node, name: str) -> list[Node] :
    """
    collect the children of n that have expr_name name and that are not
    under an expr_name node
    """
    if n.expr_name == name : return [n]
    res = []
    for x in n.children :
        res += collect_first_descendents(x, name)
    return res

def subj_or_obj(n: Node, part, suffix = "") -> str :
    if n.expr_name == 'var' :
        if part == IN_BODY : so_var.add(n.text)       
        return '?'+n.text+suffix
    if n.expr_name == 'pred' : return 'wd'+n.text
    if n.expr_name == 'entity' : return 'wd'+n.text
    return 'ERROR'

def pred(n: Node, part) -> tuple[str, str] :
    if n.expr_name == 'var' :
        if part == IN_BODY : p_var.add(n.text)
        return ('?'+n.text+'_p', '?'+n.text+'_ps')
    if n.expr_name == 'pred' : 
        if '_' in n.text:
            pp = n.text.split('_')
            pp[1] = PROP_PATTERN.sub('(p:\\1/ps:\\1)', pp[1])
            return ('p'+pp[0], 'ps'+pp[0]+'/'+pp[1])
        else:
            return ('p'+n.text, 'ps'+n.text)
    if n.expr_name == 'entity' : return ('p'+n.text, 'ps'+n.text)
    return ('error', 'error')

def statement(st: Node, id: int) :
    #print(st)
    s = 'NO_SUBJECT'
    p = 'NO_PREDICATE'
    o = 'NO_OBJECT'
    for c in st.children :
        if c.expr_name == 'subj':
            s = subj_or_obj(c.children[0], IN_BODY)
        if c.expr_name == 'obj':
            o = subj_or_obj(c.children[0], IN_BODY)
        if c.expr_name == 'predicate':
            p = pred(c.children[0], IN_BODY)
    res = '    '+ s +' '+ p[0] + '  ?stmt_' + str(id) + ' .' + '  ?stmt_'+ str(id) +'  '+ p[1] +'  '+ o
    res += process_ctxt(st)
    res += '.\n'
    return res



empty_values = [  # V, C, S, A, P
   '?__empty_v', '?__empty_c', '?__empty_s', '?__empty_a', '?__empty_p']
"""  'emptyValidity()', 'emptyCause()', 'emptySequence()', 'emptyAnnotations()', 'emptyProvenance()' ]

    '\'{"time": {"period": "", "startPeriod": "", "endPeriod": "", "start": "", "end": ""}, "space": {"place": "", "country": ""}}\'',
    '\'{"endCause": [], "hasCause": []}\'',
    '\'{"replaces": [], "replacedby": []}\'',
    '\'{"time": [], "class": [], "relation": []}\''  # for test purposes, will be changed
    ''
]
"""
categ_properties = ["pq:validityJ", "pq:causalityJ", "pq:sequenceJ", "pq:annotationJ", "pq:provenanceJ"]

def categ_variable(c: Node, pos: int):
    cvar = c.children[0].text
    if cvar == "ε":
        return ('; '+ categ_properties[pos] + ' ' + empty_values[pos])
    else:
        return ('; '+ categ_properties[pos]+' ?'+cvar)

"""
def ctxt0(c: Node) -> str : # ctxt = ("c:" / "v:" / "p:" / "s:") sp? var
    ctype =  c.children[0].text
    cvar = c.children[2].text
    if ctype == 'v:' : return '; pq:validityJ ?'+cvar
    if ctype == 'c:' : return '; pq:causalityJ ?'+cvar
    if ctype == 'p:' : return '; pq:provenanceJ ?'+cvar
    if ctype == 's:' : return '; pq:sequenceJ ?'+cvar
    if ctype == 'a:' : return '; pq:annotationJ ?'+cvar
"""

def process_statements(n: Node) :
    res = ""
    sts = collect_first_descendents(n, 'statement')
    i = 0
    for st in sts :
        res += statement(st, i)
        i += 1
    return res

def process_ctxt(n: Node) -> str :
    ctxts = collect_first_descendents(n, 'ctxt')
    res = ""
    if len(ctxts) != 5 :
        print("**** error : wrong number of sorts")
    for i in range(len(ctxts)):
        cx = ctxts[i]
        res += categ_variable(cx, i)
    return res

def process_conditions(n: Node, part) :
    if part == IN_BODY : indent = '   '
    else: indent = '              '
    conds = collect_first_descendents(n, 'condition')
    for c in conds:
        fc = process_funcall(c, part)
        print(indent+'FILTER('+fc+')')
    """
    if n.expr_name == 'condition' :
        fc = process_funcall(n, part)
        print('   FILTER('+fc+')')
    else:
        for nn in n.children : process_conditions(nn, part)
    """
    
def process_expr(n: Node, part) -> str : # var / string / funcall
    assert n.expr_name == 'expr'
    c: Node = n.children[0]
    if c.expr_name == 'var' :
        if part == IN_BODY : so_var.add(c.text) 
        return '?'+c.text
    if c.expr_name == 'string' : return c.text
    if c.expr_name == 'pred' : return 'wd'+n.text
    if c.expr_name == 'entity' : return 'wd'+n.text
    else:
        return process_funcall(c, part)

def process_funcall(n: Node, part) -> str : # funcall = funct sp? "(" arguments? ")"
    fname = n.children[0].text
    if fname not in ["str"] : fname = 'extfn:'+fname
    res = fname + '(' 
    # look for arguments
    args = collect_first_descendents(n, 'arguments')
    for a in args : res += process_arguments(a, part)
    res += ')'
    return res

def process_arguments(n: Node, part) -> str : # arguments = expr (sp expr)*
    firstarg: Node = n.children[0]
    res = process_expr(firstarg, part)
    if len(n.children) > 1 :
        others = n.children[1]
        for arg in others.children :
            argexpr = arg.children[1]
            if argexpr.expr_name != 'expr':
                print("******** error "+str(argexpr))
            res += ','+process_expr(argexpr, part)
    return res

"""
These functions are for processing statements and conditions in the head part
(inferred statements and conditions)
"""

def is_variable(n: Node) -> boolean :
    """
    n is an expr Node, it is a variable if its first child is named 'var' 
    """
    return  len(n.children) > 0 and n.children[0].expr_name == 'var'

def is_pred_or_entity(n: Node) -> boolean :
    """
    n is an expr Node, it is a predicate or entity if its first child is named 'pred' or 'entity' 
    """
    return  len(n.children) > 0 and (n.children[0].expr_name == 'pred' or n.children[0].expr_name == 'entity')

def is_pred(n: Node) -> boolean :
    """
    n is an expr Node, it is a predicate if its first child is named 'pred' 
    """
    return  len(n.children) > 0 and (n.children[0].expr_name == 'pred')

def varname(n: Node) -> str :
    """
    variable name for an expression made of a single variable
    """
    return n.children[0].text

def pred_or_entityname(n: Node) : return varname(n)

def isVariableNotInBody(n: Node) -> boolean :
    return n.expr_name == 'var' and n.text not in so_var and n.text not in p_var 

def inferredstatement(ist: Node, num: str) : # st is a inferredstatement Node
    """
    Principle: A head statement S(es, ep, eo, context) is compiled as
    
       ?__si ?_p_pi _:bi . _:bi ?_p_psi ?__oi
       BIND(translate(es) AS ?__si
       BIND(p(translate(ep)) AS ?__p_pi
       BIND(ps(translate(ep)) AS ?__p_psi
       BIND(translate(eo) AS ?__oi
       
       if es (or eo) is a head variable v ?s__si is replaced by a blank  _:__v in the graph pattern
       
       """
    # print(ist)
    exs = collect_first_descendents(ist, 'expr')
    s = '?__s'+num
    # head variable -> blank node
    if is_variable(exs[0]) :
        vn = varname(exs[0])
        if vn not in p_var and vn not in so_var : # not a body variable
            s = '_:__'+vn
            
    p1 = '?__p'+num
    p2 = '?__ps'+num
    # head variables are not allowed in predicate position
    
    o = '?__o'+num
    # head variable -> blank node
    if is_variable(exs[2]) :
        vn = varname(exs[2])
        if vn not in p_var and vn not in so_var : # not a body variable
            o = '_:__'+vn
    #if isVariableNotInBody(oe) : o = '_:__'+oe.text

    print('    '+s+' '+p1+' _:b'+num,' . _:b'+num+' '+p2+' '+o)
    ctxts = collect_first_descendents(ist, 'infctxt')
    for i in range(len(ctxts)) : 
        cx = ctxts[i]
        print(infctxt(cx, i, num))
    print('  .')


def infctxt(c: Node, pos: int, suffix: str, indent = None) -> str :  # infctxt =  expr
    margin = "  "
    if indent != None : margin = indent
    #ctxt_expr = {}
    #ctype =  c.children[0].text
    return (margin+'; '+categ_properties[pos]+' ?_x_'+c_types[pos]+suffix)
    """
    if ctype == 'v:' : print(i+'; pq:validityJ ?__v'+suffix)
    if ctype == 'c:' : print(i+'; pq:causalityJ ?__c'+suffix)
    if ctype == 'p:' : print(i+'; pq:provenanceJ ?__p'+suffix)
    if ctype == 's:' : print(i+'; pq:sequenceJ ?__s'+suffix)
    if ctype == 'a:' : print(i+'; pq:annotationJ ?__a'+suffix)
    """

def infspobind(ist: Node, suffix: str):
    """
    Generate the BIND(expression AS ?__...) for the variables in the graph pattern
    """
    #print('infspobind =============', ist)
    """
    ist: an inferredstatement
    the first three expr descendants are s p o
    """
    exs = collect_first_descendents(ist, 'expr')

    # Subject
    cex = process_expr(exs[0], IN_HEAD)
    #print(exs[0])
    if is_variable(exs[0]) :
        v = exs[0].children[0].text
        if v in so_var :
            print('   BIND(?'+v+' AS ?__s'+suffix+')')
        elif v in p_var :
            print('   BIND(extfn:p2so(?'+v+'_p) AS ?__s'+suffix+')')
        else: # head variable -> blank node -> no binding
            pass
            #print("**** syntax error, head variable not in rule body") 
    else : print('   BIND('+cex+' AS ?__s'+suffix+')')
    
    # Predicate
    cex = process_expr(exs[1], IN_HEAD)
    if is_variable(exs[1]) :
        v = exs[1].children[0].text
        if v in p_var :
            print('   BIND(?'+v+'_p AS ?__p'+suffix+')')
            print('   BIND(?'+v+'_ps AS ?__ps'+suffix+')')
        elif v in so_var :
            print('   BIND(extfn:so2p(?'+v+') AS ?__p'+suffix+')')
            print('   BIND(extfn:so2ps(?'+v+') AS ?__ps'+suffix+')')
        else:
            print("**** syntax error, head variable in predicate position") 
    elif is_pred_or_entity(exs[1]) :
        pename = pred_or_entityname(exs[1])
        if not(is_pred(exs[1]) and '_' in pename):
            p1 = "p:"+pename
            p2 = "ps:"+pename
            print('   BIND(p'+pename+' AS ?__p'+suffix+')')
            print('   BIND(ps'+pename+' AS ?__ps'+suffix+')')
    else :
        print('   BIND('+cex+' AS ?__p'+suffix+')')
        print('   BIND('+cex+' AS ?__ps'+suffix+')')
    
    # Object
    cex = process_expr(exs[2], IN_HEAD)
    if is_variable(exs[2]) :
        v = exs[2].children[0].text
        if v in so_var :
            print('   BIND(?'+v+' AS ?__o'+suffix+')')
        elif v in p_var :
            print('   BIND(extfn:p2so(?'+v+'_p) AS ?__o'+suffix+')')
        else:  # head variable -> blank node -> no binding
            pass
            # print("**** syntax error, head variable not in rule body") 
    else : 
        print('   BIND('+cex+' AS ?__o'+suffix+')')
    
    
def infctxtbind(c: Node, suffix = "") : # infctxt = ("c:" / "v:" / "p:" / "s:") sp? expr
    ctxts = collect_first_descendents(c, 'infctxt')
    for i in range(len(ctxts)) :
        cx = ctxts[i]
        ctype = c_types[i]
        if cx.children[0].text == 'ε':
            cex = empty_values[i]
        else:
            cex = process_expr(cx.children[0], IN_HEAD)
        print('   BIND('+cex+' AS ?_x_'+ctype+suffix+')')

def set_pred_or_entity_prefix(n: Node):
    if n.expr_name == 'pred' : return 'wd'+n.text
    if n.expr_name == 'entity' : return 'wd'+n.text
    
def fnex(ist: Node, num: int) -> str : # ist is a inferredstatement Node
    """
    Generate a FILTER NOT EXISTS condition 
    - to avoid generating twice the same (or blank-equivalent) graph pattern (infereence rules)
    - to detect subgraphs that do not satisfy a constraint
    It's essentiallly the same graph pattern as the one generated for the CONSTRUCT, with blanks
    replaced by variables for head variables.
    """
    res = ""
    exs = collect_first_descendents(ist, 'expr')
    s = '?__s'+str(num)
    # head variable -> keep it as is
    if is_variable(exs[0]) :
        vn = varname(exs[0])
        if vn not in p_var and vn not in so_var : # not a body variable
            s = '?'+vn
            head_so_var.add(vn)
    pred_is_path_expression = False    
    p1 = '?__p'+str(num)
    p2 = '?__ps'+str(num)
    
    # head variable -> keep it as is
    if is_variable(exs[1]) :
        vn = varname(exs[1])
        if vn not in p_var and vn not in so_var : # not a body variable
            p1 = '?'+vn+'_p'
            p2 = '?'+vn+'_ps'
            head_p_var.add(vn)
    elif is_pred_or_entity(exs[1]) :
        pename = pred_or_entityname(exs[1])
        p1 = "p"+pename
        p2 = "ps"+pename
        if is_pred(exs[1]) :
            pn = varname(exs[1])
            if '_' in pn:
                pred_is_path_expression = True
                pp = pn.split('_')
                p1 = 'p'+pp[0]
                p2 = 'ps'+pp[0]+'/'+PROP_PATTERN.sub('(p:\\1/ps:\\1)', pp[1])
            
    o = '?__o'+str(num)
    # head variable -> keep it as is
    if is_variable(exs[2]) :
        vn = varname(exs[2])
        if vn not in p_var and vn not in so_var : # not a body variable
            o = '?'+vn
            head_so_var.add(vn)

    res += '            '+s+' '+p1+' _:c'+str(num)+' . _:c'+str(num)+' '+p2+' '+o+'\n'
    ctxts = collect_first_descendents(ist, 'infctxt')
    for i in range(len(ctxts)) : res += infctxt(ctxts[i], i, str(num), '            ') + '\n'
    res += '            .\n'
    return res

def make_variables_equivalent(pred_var: set[str], subjobj_var: set[str]) -> str :
    res = ""
    for v in pred_var:
        res += '  BIND(IRI(REPLACE(STR(?'+v+'_p), "/prop/", "/prop/statement/")) AS ?'+v+'_ps)\n'
        if v in so_var :
            res += '  BIND(IRI(REPLACE(STR(?'+v+'), "/entity/", "/prop/")) AS ?'+v+'_p)\n'
    return res
            
def equalize_variables():
    for v in p_var:
        print('   FILTER(STRAFTER(?'+v+'_p, "/prop/")=STRAFTER(?'+v+'_ps, "/statement/"))')
        if v in so_var :
            print('   FILTER(STRAFTER(?'+v+'_p, "/prop/")=STRAFTER(?'+v+', "/entity/"))')
        # reserve: /qualifier/

prefixes = """
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX extfn: <http://www.ontotext.com/js#>

"""

def rule(r: Node):
    
    print(prefixes)
    compiled_stmt = process_statements(r)
    #print('Body variables:',so_var,p_var)
    infstatements = collect_first_descendents(r, 'inferredstatement')
    if category == RULE:
        print('CONSTRUCT {')
        for i, infs in enumerate(infstatements):
            inferredstatement(infs, str(i))  # generate CONSTRUCT ...
        print('}')
    else:
        print('SELECT * ')
    print('WHERE {')
    print("""
   BIND(extfn:emptyValidity() AS ?__empty_v)
   BIND(extfn:emptyCause() AS ?__empty_c)
   BIND(extfn:emptySequnce() AS ?__empty_s)
   BIND(extfn:emptyAnnotations() AS ?__empty_a)
   BIND(extfn:emptyProvenance() AS ?__empty_p)
    """)
    """
    '\'{"time": {"period": "", "startPeriod": "", "endPeriod": "", "start": "", "end": ""}, "space": {"place": "", "country": ""}}\'',
    '\'{"endCause": [], "hasCause": []}\'',
    '\'{"replaces": [], "replacedby": []}\'',
    '\'{"time": [], "class": [], "relation": []}\''  # for test purposes, will be changed
    ''
    """
    print(make_variables_equivalent(p_var, so_var))
    print(compiled_stmt)
    body = collect_first_descendents(r, 'body')[0]
    process_conditions(body, IN_BODY)
    
    for i, infs in enumerate(infstatements):
        infspobind(infs, str(i))
        infctxtbind(infs, str(i))     
    print('\n          FILTER NOT EXISTS {')
    graph_pattern = ""
    for i, infs in enumerate(infstatements):
        graph_pattern += fnex(infs, i)  
    print(make_variables_equivalent(head_p_var, head_so_var))
    print(graph_pattern)
    head = collect_first_descendents(r, 'head')[0]
    process_conditions(head, IN_HEAD)   
    print('          }')
    print('}')

def read_rule() -> str :
    if len(sys.argv) != 2:
        raise ValueError('usage: python3 rule-compiler.py file-name')

    rfilename = sys.argv[1]
    rfile = open(rfilename)
    rule = rfile.read()
    return rule

def start() :
    global category
    
    r = read_rule()
    
    try:
        pp = g.parse(r)
        cat = pp.children[0].children[0].text
        if cat == 'constraint' : category = CONSTRAINT  # else RULE
        print('# Generated by MSL-rule compiler for Wikidata, v. 1.0 beta')
        print('#')
        rcomment = '# '+r.replace('\n','\n# ')
        print(rcomment)
        
        rr = collect_first_descendents(pp, "rule")[0]
   
        rule(rr)
    except parsimonious.exceptions.ParseError as stxerror:
        print("Syntax error")
        print(stxerror)
        

start()
