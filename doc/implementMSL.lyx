#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\begin_modules
theorems-ams
\end_modules
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures true
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\use_minted 0
\index Index
\shortcut idx
\color #008000
\end_index
\leftmargin 1.5cm
\rightmargin 1.5cm
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Introduction
\end_layout

\begin_layout Standard
It is widely acknowledged that in large knowledge bases most of the expressed
 knowledge (from simple facts to complex domain axioms) needs contextualization
 information.
 In Wikidata this information is attached to each statement via so-called
 qualifiers.
\end_layout

\begin_layout Standard
Contextual information (in this case qualifier) must be taken into account
 when querying the knowledge base or when making inferences but it must
 be handled differently depending on the nature of the context.
 For instance, if 
\begin_inset Formula $P_{1}$
\end_inset

 has a temporal validity context 
\begin_inset Formula $T_{1}$
\end_inset

 and 
\begin_inset Formula $P_{2}$
\end_inset

 has a temporal l validity context 
\begin_inset Formula $T_{2}$
\end_inset

 then 
\begin_inset Formula $P_{1}\land P_{2}$
\end_inset

 is certainly valid in 
\begin_inset Formula $T_{1}\cap T_{2}$
\end_inset

.
 On the other hand, if if 
\begin_inset Formula $P_{1}$
\end_inset

 has a causality context 
\begin_inset Formula $C_{1}$
\end_inset

 and 
\begin_inset Formula $P_{2}$
\end_inset

 has a causality context 
\begin_inset Formula $C_{2}$
\end_inset

 then then causality of 
\begin_inset Formula $P_{1}\land P_{2}$
\end_inset

 is probably better described as 
\begin_inset Formula $C_{1}\cup C_{2}$
\end_inset

.
\end_layout

\begin_layout Standard
The problem with Wikidata qualifiers is that they are not structured or
 grouped according to contextual dimensions such as valididty, causality,
 provenance, etc.
 Moreover, there are numerous, partly redundant, qualifiers.
 A validity context can be described by its start and end time, or by a
 point in time (e.g.
 a year, which is in fact a temporal interval), or a validity period (e.g.
 summer).
 Therefore it is extremely complex to write correct contextual queries directly
 on the Wikidata graph (even with MARS & co.).
 Such queries involve multiple qualifiers, that can be defined or undefined,
 etc.
\end_layout

\begin_layout Standard
To address this problem we propose to build an abstraction layer on top
 of contextual qualifiers.
\end_layout

\begin_layout Standard
...
\end_layout

\begin_layout Standard
In the MSL vision each statement has several contextual dimensions: validity,
 causality, provenance, ...
 .
 Each context c in a dimension d is represented by a value of a sort sd
 associated to d.
 The question is: How to represent the values of each sort and how to implement
 the sort operations?
\end_layout

\begin_layout Subsection*
The contextualization process
\end_layout

\begin_layout Paragraph
Specification
\end_layout

\begin_layout Enumerate
identify the relevant contextual dimensions (for the application) 
\end_layout

\begin_layout Enumerate
specify the sorts corresponding to the dimensions (define the operations
 on the sorts by their signatures and logical axioms) 
\end_layout

\begin_layout Enumerate
specify the contextual inference rules in MSL using the sorts and sort operation
s 
\end_layout

\begin_layout Paragraph
Implementation
\end_layout

\begin_layout Enumerate
for each sort (contextual dimension) 
\end_layout

\begin_deeper
\begin_layout Enumerate
find all the qualifiers that are used in WD to represent values (contexts)
 of this sort 
\end_layout

\begin_layout Enumerate
define a mapping from sets of (qualifier, object) pairs to sort values.
 Note that in practice this mapping may be very complex because 1) a dimension,
 such as 
\emph on
validity
\emph default
 may have a complex structure (time+space valididty); 2) the same context
 may be expressed in different ways (time interval = start + end or start
 + duration); 3) some values are implicitly specified (the absence of end
 time qualifier can mean 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

"
\end_layout

\end_inset

up to now
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

"
\end_layout

\end_inset

 or 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

"
\end_layout

\end_inset

forever
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

"
\end_layout

\end_inset

 or 
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

"
\end_layout

\end_inset

unknown
\begin_inset ERT
status collapsed

\begin_layout Plain Layout

"
\end_layout

\end_inset

) 
\end_layout

\end_deeper
\begin_layout Enumerate
define a representation for the sorts 
\end_layout

\begin_layout Enumerate
define an implementation for the sort operations (in JS, SPARQL, ...) 
\end_layout

\begin_layout Enumerate
implement the mappings from qualifiers to sort values 
\end_layout

\begin_layout Enumerate
rewrite or compile the MSL inference rules into executable expressions in
 a SW language (SPARQL, SHACL, ...) + calls to sort operations
\end_layout

\begin_layout Paragraph
The literal approach to sort representation
\end_layout

\begin_layout Standard
In this approach each value is represented by a RDF literal.
 We chose to use JSON strings but other encoding would also be suitable.
 Since contextual information in WD is carried by qualifiers (e.g.
 the temporal validity can be indicated by P580 start time, P582 end time,
 P1264 valid in period, etc.
 )
\end_layout

\begin_layout Section
Mapping qualifier values to sort values
\end_layout

\begin_layout Standard
The first implementation step consists in constructing the sort values for
 the different context sorts.
 For a statement s and for each context sort it consists in mapping the
 subgraph that represents this context sort value for s to a JSON value
 (object, array, number, or string, or one of false null true) that represents
 a sort value.
\end_layout

\begin_layout Standard
Example, the causality contxt of a statement 
\begin_inset Formula $S$
\end_inset

 is representet by the subgraph made of all the triples 
\begin_inset Formula $(S,p,o)$
\end_inset

 where 
\begin_inset Formula $p$
\end_inset

 is either 
\begin_inset Formula $P828$
\end_inset

 (has cause) or 
\begin_inset Formula $P1534$
\end_inset

 (has cause).
 A possible mapping can be
\end_layout

\begin_layout Standard
If 
\begin_inset Formula $(S,\texttt{pq:P828},h_{1}),...,(S,\texttt{pq:P828},h_{k})$
\end_inset

 are all the triples with subject 
\begin_inset Formula $S$
\end_inset

 and predicate 
\family typewriter
pq:P828
\family default
 and an non-blank object, and 
\begin_inset Formula $(S,\texttt{pq:P1534},e_{1})$
\end_inset

, ..., 
\begin_inset Formula $(S,\texttt{pq:P1534},e_{n})$
\end_inset

 are all the triples with subject 
\begin_inset Formula $S$
\end_inset

 and predicate 
\family typewriter
pq:P1534
\family default
 and a non-blank object, then the corresponding JSON object is
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
\texttt{\{"hasCause": [\ensuremath{h_{1}'}, \ensuremath{h_{2}'}, ..., \ensuremath{h_{k}'}], "endCause": [\ensuremath{e_{1}'}, \ensuremath{e_{2}'}, ..., \ensuremath{e_{n}'}]\}}
\]

\end_inset


\end_layout

\begin_layout Standard
where 
\begin_inset Formula $h_{i}'$
\end_inset

 (resp.
 
\begin_inset Formula $e_{i}'$
\end_inset

) is a string representation of the node 
\begin_inset Formula $h_{i}$
\end_inset

 (resp.
 
\begin_inset Formula $e_{i}$
\end_inset

)
\end_layout

\begin_layout Standard
This mapping can be implemented in different ways:
\end_layout

\begin_layout Enumerate
with a (python) script that reads all the statements of interest in the
 graph, computes the representation and stores it in the 'context' graph
 
\end_layout

\begin_layout Enumerate
with a SPARQL INSERT statement, if the value computation is algorithmically
 simple 
\end_layout

\begin_layout Enumerate
with a SPARQL INSERT + calls to SPARQL/SHACL functions that correspond to
 the constructor functions of the target sort 
\end_layout

\begin_layout Section
Storing the sort values
\end_layout

\begin_layout Standard
The mapping results can be stored in a 
\begin_inset Quotes eld
\end_inset

context
\begin_inset Quotes erd
\end_inset

 graph that has for each statement s and each context dimension c a triple
 (s c-context jv), where jv is the JSON object resulting from the mapping
 of the qualifiers that represent c for s.
\end_layout

\begin_layout Paragraph
Remark about McCarthy's principle.
\end_layout

\begin_layout Standard
According to McCarthy a context must be an object.
 The literal approach satisfies this condition if and only if two different
 context are always represented by different values.
 This is the case, for instance, for time validity contexts that are represented
 by time intervals with known boundaries.
 If the boundaries are allowed to be unknown, or fuzzy, then two intervals
 
\begin_inset Formula $T_{1}$
\end_inset

 and 
\begin_inset Formula $T_{2}$
\end_inset

 may have the same representation, say 
\family typewriter
[unknown, 2030]
\family default
 without being the same.
 We may also have situations in which the boundaries of 
\begin_inset Formula $T_{1}$
\end_inset

 and 
\begin_inset Formula $T_{2}$
\end_inset

 are totally unknown but we know that 
\begin_inset Formula $T_{1}$
\end_inset

 occurs before 
\begin_inset Formula $T_{2}$
\end_inset

.
\end_layout

\begin_layout Standard
To handle such cases, each JSON object that represents a context can be
 augmented with an attribute @id that uniquely identifies the object.
\end_layout

\begin_layout Section
Performing inferences
\end_layout

\begin_layout Standard
Inference rules are built with the S (statement) predicate and the predicates
 and operations defined on the contextual sorts (in the corresponding modules).
 It is possible to represent these rules as SHACL rules or as SPARQL CONSTRUCT
 statements provided the sort operations are implemented as external SHACL/SPARQ
L functions.
 This can typically be done by writing and registering Javascript or SPARQL
 functions.
\end_layout

\begin_layout Standard
Since the sort values are JSON values stored in strings (literals), the
 functions typically follows the pattern
\end_layout

\begin_layout Verbatim

    function op(string_p1, string_p2, ...){
\end_layout

\begin_layout Verbatim

      var p1 = JSON.parse(string_p1)
\end_layout

\begin_layout Verbatim

      var p2 = JSON.parse(string_p2)
\end_layout

\begin_layout Verbatim

      ...
\end_layout

\begin_layout Verbatim

      // do the op computation
\end_layout

\begin_layout Verbatim

      result = ...
\end_layout

\begin_layout Verbatim

      string_result = JSON.stringify(result)
\end_layout

\begin_layout Verbatim

      return string_result
\end_layout

\begin_layout Verbatim

    }
\end_layout

\begin_layout Example*
Consider the rule
\end_layout

\begin_layout LyX-Code
S(p, <<property_constraint>>, <<transitive_constraint>>, null, null, null,
 A0, P0)
\end_layout

\begin_layout LyX-Code
^ S(x1, p, y1, V1, C1, S1, A1, P1)
\end_layout

\begin_layout LyX-Code
^ S(y1, p, z1, V2, C2, S2, A2, P2)
\end_layout

\begin_layout LyX-Code
^ testIntersect(V1, V2)
\end_layout

\begin_layout LyX-Code
-> S(x, p, z, inter(V1, V2), union(C1, C2), null, null, union(P1, P2))
\end_layout

\begin_layout Standard
(<<property_constraint>> and <<transitive_constraint>> are the wikidata
 properties Pxxxx...)
\end_layout

\begin_layout LyX-Code
   
\end_layout

\begin_layout Standard
It can be implemented on the Wikidata RDF dump graph as a CONSTRUCT statement
\end_layout

\begin_layout Verbatim

CONSTRUCT {?x1 ?p_p1 [ ?p_p2 ?z1; validityJ ?hvc ; 
\end_layout

\begin_layout Verbatim

                    pq:causalityJ ?hcc ; 
\end_layout

\begin_layout Verbatim

                    pq:provenanceJ ?hpc]}
\end_layout

\begin_layout Verbatim

\end_layout

\begin_layout Verbatim

WHERE { ?p p:<<property constraint>> ?s_1 .
\end_layout

\begin_layout Verbatim

           ?s_1 ps:<<property constraint>> wd:<<transitive constraint>>
 .
\end_layout

\begin_layout Verbatim

        ?x1 ?p_p1 ?s_2.
 ?s_2 ?p_p2 ?y1 ; 
\end_layout

\begin_layout Verbatim

           pq:validityJ ?V1 ; pq:causalityJ ?C1 ; pq:provenanceJ ?P1 .
\end_layout

\begin_layout Verbatim

        ?y1 ?p_p1 ?s_3.
 ?s_3 ?p_p2 ?z1 ; 
\end_layout

\begin_layout Verbatim

           pq:validityJ ?V2 ; pq:causalityJ ?C2 ; pq:provenanceJ ?P2 .
\end_layout

\begin_layout Verbatim

\end_layout

\begin_layout Verbatim

        FILTER(after_wd_prefix(?p) = after_p_prefix(?p_p1))  # see WD RDF
 dump
\end_layout

\begin_layout Verbatim

        FILTER(after_wd_prefix(?p) = after_ps_prefix(?p_p2)) # ...
\end_layout

\begin_layout Verbatim

\end_layout

\begin_layout Verbatim

        FILTER(ext:testIntersect(?V1, ?V2))
\end_layout

\begin_layout Verbatim

\end_layout

\begin_layout Verbatim

        BIND(ext:validity_inter(?V1, ?V2) as ?hvc)
\end_layout

\begin_layout Verbatim

        BIND(ext:causality_union(?C1, ?C2) as ?hcc)
\end_layout

\begin_layout Verbatim

        BIND(ext:provenance_union(?P1, ?P2) as ?hpc)
\end_layout

\begin_layout Verbatim

        }
\end_layout

\begin_layout Verbatim

    
\end_layout

\begin_layout Standard
This example provides an intuition on how a rule compiler could transform
 MSL rules into executable code, in SPARQL, in a very straightforward manner.
\end_layout

\begin_layout Standard
In the RDF representation of WD the same entity may have different IRIs,
 depending on its role in a statement representation.
 For instance, the property P19 (place of birth) is represented as wd:P19
 when it is the subject or object of a statement, and by p:P19 and ps:P19
 when it is the predicate.
 Therefore, if the same variable appears in different positions in a rule,
 it must be represented by several variables in the SPARQL expression (
\begin_inset Formula $?p$
\end_inset

, 
\begin_inset Formula $?p\_p1$
\end_inset

, 
\begin_inset Formula $?p\_p2$
\end_inset

 in our example) and a filter must ensure that they have the same value
 after their prefixes.
\end_layout

\begin_layout Subsection*
Recursive rules and termination
\end_layout

\begin_layout Standard
In fact this rule is recursive, it can create new facts that are used by
 the same rule.
 So a complete implementation must store the inferred triples in the graph,
 or in an graph of the dataset, and iteracte up to saturation.
 It can also be directly written as a SHACL rule if the target triple store
 has advanced SHACL capabilities such as inference support.
\end_layout

\begin_layout Standard
Moreover, since the S(...) predicate in the rule's head is represented by a
 subgraph having a new blank node, the same S(...) statement (with the same
 s, p, o, context values) may be inferred multiple times.
 This could lead to infinite loops in the inference process.
 So, before creating a new S(...), the inference system must check that an
 equivalent (up to blank node) statement representation does not already
 exist in the graph.
 This can be done by incorporating an additional filter in the SPARQL CONSTRUCT
 for the rule:
\end_layout

\begin_layout Verbatim

\end_layout

\begin_layout Verbatim

CONSTRUCT {?x1 ?p_p1 [ ?p_p2 ?z1; pq:validityJ ?hvc ; 
\end_layout

\begin_layout Verbatim

                    pq:causalityJ ?hcc ; 
\end_layout

\begin_layout Verbatim

                    pq:provenanceJ ?hpc]}
\end_layout

\begin_layout Verbatim

WHERE { ...
\end_layout

\begin_layout Verbatim

        # avoid multiple inferences of equivalent statements
\end_layout

\begin_layout Verbatim

        FILTER NOT EXISTS 
\end_layout

\begin_layout Verbatim

          {?x1 ?p_p1 [ ?p_p2 ?z1; pq:validityJ ?hvc_ ; 
\end_layout

\begin_layout Verbatim

                    pq:causalityJ ?hvc_ ; 
\end_layout

\begin_layout Verbatim

                    pq:provenanceJ ?hpc_] 
\end_layout

\begin_layout Verbatim

           FILTER( extfn:validity_equal(?hvc, ?hvc_) && 
\end_layout

\begin_layout Verbatim

                   extfn:causality_equal(?hvc, ?hvc_) && 
\end_layout

\begin_layout Verbatim

                   ext:provenance_equal(?hpc, ?hpc_))}
\end_layout

\begin_layout Verbatim

        }
\end_layout

\begin_layout Verbatim

\end_layout

\begin_layout Verbatim

    
\end_layout

\begin_layout Section
Compiling MSL Rules
\end_layout

\begin_layout Subsection
Syntax
\end_layout

\begin_layout Standard
An MSL rule has the genral form 
\begin_inset Formula $B_{1},B_{2},...,B_{n}\to H$
\end_inset

 where each 
\begin_inset Formula $B_{i}$
\end_inset

 can take two forms: 
\end_layout

\begin_layout Enumerate
\begin_inset Formula $S(x,p,y,c_{1},...,c_{k})$
\end_inset

 to represent wikipedia statements.
 The terms 
\begin_inset Formula $x,p,y,c_{1},...,c_{k}$
\end_inset

 are either constants (IRI or literals) or variables.
 
\end_layout

\begin_layout Enumerate
\begin_inset Formula $pred(t_{1},...,t_{m})$
\end_inset

 for predicates on the variables and constants appearing in the statements.
 The terms 
\begin_inset Formula $t_{1},...,t_{m}$
\end_inset

 are either operation applications 
\begin_inset Formula $op(u_{1},...,u_{n})$
\end_inset

 or variables or constants
\end_layout

\begin_layout Standard
The head (
\begin_inset Formula $H$
\end_inset

) takes the same form as the body statements but the terms can be expressions,
 not just constants and variables.
 
\end_layout

\begin_layout Subsection*
Compiling a statement atom 
\end_layout

\begin_layout Standard
A statement 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
S(x,p,y,c_{1},...,c_{k})
\]

\end_inset

is compiled into the graph pattern with filters: 
\begin_inset Formula 
\[
\begin{array}{c}
[x]\ [y]_{p1}?s_{i}.?s_{i}\ [y]_{p2}\ [z].\ filter([y]_{p},[y]_{ps})\\
?s_{i}\ \texttt{pq:context}_{1}\ [c_{1}].\ldots?s_{i}\ \texttt{pq:context}_{k}\ [c_{k}].
\end{array}
\]

\end_inset

where 
\begin_inset Formula 
\[
[t]=\left\{ \begin{array}{ll}
?v & \text{if \ensuremath{t} is a token that denotes a variable named \ensuremath{v}}\\
c & \text{if \ensuremath{t} denotes a constant \ensuremath{c}}
\end{array}\right.
\]

\end_inset


\begin_inset Formula 
\[
[u]_{p1}=\left\{ \begin{array}{ll}
?v\texttt{\_p1} & \text{if \ensuremath{t} is a token that denotes a variable named \ensuremath{v}}\\
\texttt{p:}c & \text{if \ensuremath{t} denotes a constant \ensuremath{c}}
\end{array}\right.
\]

\end_inset


\begin_inset Formula 
\[
[u]_{p2}=\left\{ \begin{array}{ll}
?v\texttt{\_p2} & \text{if \ensuremath{t} is a token that denotes a variable named \ensuremath{v}}\\
\texttt{ps:}c & \text{if \ensuremath{t} denotes a constant \ensuremath{c}}
\end{array}\right.
\]

\end_inset


\begin_inset Formula 
\[
s_{i}\text{ is a new variable not yet used in the compilation process}
\]

\end_inset


\end_layout

\begin_layout Standard
If a variable 
\begin_inset Formula $v$
\end_inset

 appears in predicate position in some statement, a filter is added to the
 graph patterns:
\begin_inset Formula 
\[
\mathtt{FILTER(STRAFTER(}\ensuremath{px,?v\texttt{\_p1}}\mathtt{)=STRAFTER(}\ensuremath{psx,?v\texttt{\_p2}}\mathtt{)}
\]

\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
where 
\begin_inset Formula $px$
\end_inset

 (resp.
 
\begin_inset Formula $psx$
\end_inset

) is the expansion of the 
\begin_inset Formula $\texttt{p:}$
\end_inset

 (resp.
 
\begin_inset Formula $\texttt{ps:}$
\end_inset

) prefix.
\end_layout

\begin_layout Standard
If a variable 
\begin_inset Formula $v$
\end_inset

 appears in subject or object position and in predicate position in some
 statements, a filter is added to the graph patterns:
\begin_inset Formula 
\[
\mathtt{FILTER(STRAFTER(}\ensuremath{px,?v\texttt{\_p1}}\mathtt{)=STRAFTER(}\ensuremath{wdx,?v}\mathtt{)}
\]

\end_inset


\family roman
\series medium
\shape up
\size normal
\emph off
\bar no
\strikeout off
\xout off
\uuline off
\uwave off
\noun off
\color none
where 
\begin_inset Formula $px$
\end_inset

 (resp.
 
\begin_inset Formula $wdx$
\end_inset

) is the expansion of the 
\begin_inset Formula $\texttt{p:}$
\end_inset

 (resp.
 
\begin_inset Formula $\texttt{wd:}$
\end_inset

) prefix.
\end_layout

\begin_layout Subsection*
Compiling a predicate atom
\end_layout

\begin_layout Standard
An atom 
\begin_inset Formula 
\[
p(t_{1},...,t_{m})
\]

\end_inset

 is compiled into a filter expression
\begin_inset Formula 
\[
\texttt{FILTER(extfn:}p\texttt{(}[t_{1}],...,[t_{m}]\texttt{))}
\]

\end_inset

 where 
\begin_inset Formula $[t_{i}]=\left\{ \begin{array}{ll}
\texttt{extfn:}f([u_{1}],...,[u_{k}]) & \text{if \ensuremath{t_{i}=f(u_{1},...,u_{k})} }\\
c & \text{if \ensuremath{t_{i}} denotes a constant \ensuremath{c}}\\
\text{?v} & \text{if \ensuremath{t} is a token that denotes a variable named \ensuremath{v}}
\end{array}\right.$
\end_inset


\end_layout

\begin_layout Subsection*
Compiling the head statement
\end_layout

\begin_layout Standard
A statement 
\end_layout

\begin_layout Standard
\begin_inset Formula 
\[
S(x,p,y,c_{1},...,c_{k})
\]

\end_inset

that appears in the head is compiled into a construct part 
\begin_inset Formula 
\[
\begin{array}{ccc}
\texttt{CONSTRUCT\{} & [x]\ [p]_{p}\texttt{ \_:1 . \_:1 }[p]_{ps}\ [y]\ \texttt{.}\\
 & \texttt{\_:1 pq:context}_{1}\ \texttt{?vc}_{1}\ \texttt{.}\\
 & \ldots\\
 & \texttt{\_:1 pq:context}_{k}\ \texttt{?vc}_{k}\ \texttt{.} & \texttt{\}}
\end{array}
\]

\end_inset

 and zero or more BIND expressions 
\begin_inset Formula 
\[
\texttt{BIND(\ensuremath{[c_{i}]} AS ?vc\ensuremath{_{i}})}
\]

\end_inset


\end_layout

\begin_layout Standard
TODO: add p(?x), ps(?x), wd(?x)
\end_layout

\end_body
\end_document
