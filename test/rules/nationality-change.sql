# S(x1, countryOfCitizenship, y1, V1,C1, S1,A1, P1)
# ∧ S(y1, followed by, y2, V2,C2, S2,A2, P2)
# ∧ endTime(extractTime(V1)) = getTime(A2)
# −→ S(x1, country of Citizenship, y2,
# setTime(empty, interval(getTime(A2), null)),
# addHasCause(getEndCause(C1), empty),
# null,
# null,
# union(P1, P2)
# )

PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX extfn: <http://www.ontotext.com/js#>

construct{
    ?x1 p:P27   _:1 .
    _:1 ps:P27  ?y1 .
    _:1 pq:validityJ ?VC1 .
    _:1 pq:causalityJ ?C

}
where {
    ?x1 p:P27   ?r1 .
    ?r1 ps:P27  ?y1 . 
    ?y1 p:P156  ?r2 .
    ?r2 ps:P156 ?y2 .
    ?r1 pq:validityJ ?v1 . 
    ?r2 pq:validityJ ?v2 .
    optional{?r1 pq:causalityJ ?c1_opt} bind(if(bound(?c1_opt),?c1_opt, extfn:emptyCause()) as ?c1) . 
    optional{?r2 pq:causalityJ ?c2_opt} bind(if(bound(?c2_opt),?c2_opt, extfn:emptyCause()) as ?c2) . 
    ?r2 pq:annotationJ ?a2 .
    bind(extfn:addTime(extfn:interval(extfn:getTimeAnnot(?a2), "")) as ?VC1)
    bind(extfn:addHasCause(extfn:emptyCause(), extfn:getEndCause(?c1)) as ?C)
    filter not exists {
        ?x1 p:P27   _:2 .
        _:2 ps:P27  ?y1 .
        _:2 pq:validityJ ?VC1 .
        _:2 pq:causalityJ ?C
    }

}
